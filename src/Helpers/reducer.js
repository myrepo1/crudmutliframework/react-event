export const useCombinedReducer = useReducer => {
    // Global state
    const state = Object.keys(useReducer).reduce((acc,key)=>({...acc,[key]:useReducer[key][0]}),{});
    // Global dispatch
    const dispatch = action => Object.keys(useReducer).map(key=>useReducer[key][1]).forEach(fn=>fn(action));
    return [state,dispatch];
  };