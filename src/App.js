import React, {useReducer} from 'react';
import {BrowserRouter as Router,Switch,Route} from 'react-router-dom';
import 'bootstrap/dist/css/bootstrap.min.css';
import 'react-datepicker/dist/react-datepicker.min.css';

// Layout
import Header from './Layouts/Header';
import Home from './Layouts/Home';
import Login from './Layouts/Login';
import {Events,EventForm,EventDetail} from './Layouts/Events'

// Reducer
import getAccountReducer from './States/Account/reducer';

// Addition
import StateManager from './States/context';
import {useCombinedReducer} from './Helpers/reducer';

const App=() =>{
  const [state,dispatch] = useCombinedReducer({
    account:useReducer(...getAccountReducer())
  });
  return (
    <StateManager.Provider value={{state,dispatch}}>
      <Router>
        <Header/>
        <Switch>
          <Route exact path='/'>
            <Home/>
          </Route>
          <Route path='/events'>
            <Events/>
          </Route>
          <Route path='/login'>
            <Login/>
          </Route>
          <Route path='/event/new'>
            <EventForm/>
          </Route>
          <Route path='/event/:id'>
            <EventDetail/>
          </Route>
        </Switch>
      </Router>
    </StateManager.Provider>
  );
}

export default App;
