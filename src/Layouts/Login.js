import React, {useContext} from 'react';
import {Container, Row} from 'react-bootstrap';

import Forms from '../Components/Forms';
import StateManager from '../States/context';
import {request} from '../Helpers/request';
import {successGetToken} from '../States/Account/action';

const Login = () => {
    const dispatch = useContext(StateManager).dispatch;
    const loginAttempt = async (data)=>{
        const payload = {username:data.username,password:data.password};
        console.log(payload)
        try {
            const result = await request.post('/account/login',payload);
            const token = result.data.data.access_token;
            const id = result.data.data.id;
            const is_staff = result.data.data.is_staff;
            localStorage.setItem('account',JSON.stringify({token,id,is_staff}));
            dispatch(successGetToken(token,id,is_staff));
        } catch(error) {
            const result = error.response.data;
            return {message:result.message,detail:result.detail_message};
        }
    };

    const fields = [
        {name:'username',type:'text',initial:''},
        {name:'password',type:'password',initial:''},
    ];
    
    return (
        <Container>
            <Row><h3>Login Form</h3></Row>
            <Row>
                <Forms fields={fields} formAction={loginAttempt} redirection="/"/>
            </Row>
        </Container>
    )
}

export default Login;