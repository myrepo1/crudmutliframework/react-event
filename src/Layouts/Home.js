import React, {useState,useEffect} from 'react';
import {Container,Row} from 'react-bootstrap';
import {Link} from 'react-router-dom';

import {DisplayCard} from '../Components/Cards';
import {request} from '../Helpers/request';

const Home = () => {
    const [event,setEvent] = useState([]);

    useEffect(() => {
        const getData = async () => {
            const result = await request.get('/event/events');
            setEvent(result.data.data);
        };
        getData();
    },[]);

    return (
        <Container>
            <Row><h3>Welcome to Events Gallery</h3></Row>
            <Row>
                <Link to="/event/new" className="btn btn-primary">Add Event</Link>
            </Row>
            <Row>{event.map((item,index)=>{
                const image= item.id? `/event/event/${item.id}/image`:null;
                const title=item.shortname;
                const text = ['start','location'];
                const buttons = [{className:'btn btn-primary','name':'Detail',link:`/event/${item.id}`}];
                return <DisplayCard image={image} title={title} text={text} buttons={buttons} data={item} key={index}/>
                })}
            </Row>
        </Container>
    )
}

export default Home;