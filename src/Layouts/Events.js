import Events from './Event/Events';
import EventDetail from './Event/EventDetail';
import EventForm from './Event/EventForm';

export {EventDetail,EventForm,Events};