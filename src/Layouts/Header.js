import React, {useContext, useState} from 'react';
import {Link, Redirect} from 'react-router-dom';
import {Navbar,Nav} from 'react-bootstrap';

import StateManager from '../States/context';
import {noToken} from '../States/Account/action';

const Logout = ()=>{
  const [loggedOut,setLoggedOut] = useState(false);
  const dispatch = useContext(StateManager).dispatch;

  const handleClick = async ()=>{
    localStorage.clear('account');
    await dispatch(noToken());
    setLoggedOut(true);
  }

  if(loggedOut) {
    return <Redirect to="/"/>
  } else {
    return <Nav.Link href="#" onClick={handleClick}>Logout</Nav.Link>
  }
};

const Header = ()=>{
  const account = useContext(StateManager).state.account;
    return (
      <Navbar bg="dark" expand="lg" variant="dark">
          <Link className="navbar-brand" to="/">Events Gallery</Link>
          <Navbar.Toggle aria-controls="basic-navbar-nav"/>
          <Navbar.Collapse id="basic-navbar-nav">
            <Nav className="mr-auto">
              <Link className="nav-link" to="/events">Events</Link>
              {account.is_staff?<Nav.Link href="#">Accounts</Nav.Link>:null}
              {account.token?<Logout/>:<Link className="nav-link" to="/login">Login</Link>}
            </Nav>
          </Navbar.Collapse>
        </Navbar>
    )
  }

export default Header;