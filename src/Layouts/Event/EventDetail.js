import React,{ useEffect, useState, useContext } from 'react';
import {Row,Col, Button,Container} from 'react-bootstrap';
import {useParams} from 'react-router-dom';

import StateManager from '../../States/context';
import {request} from '../../Helpers/request';
import {DetailCard} from '../../Components/Cards';

const EventDetail =()=>{
    const [detail,setDetail] = useState({});
    const image = detail.id?`/event/event/${detail.id}/image`:null;
    const text = ['shortname','start','location','description'];
    const {id} = useParams();
    const state = useContext(StateManager).state;

    useEffect(()=>{
        const getData = async () => {
            const result = await request.get(`/event/event/${id}`);
            setDetail(result.data.data);
        };
        getData()
    },[id]
    );
    return (
        <Container>
            <Row><h3>{detail.name}</h3></Row>
            <Row>
                {state.account.is_staff?
                <Col>
                <Button className="btn btn-warning">Edit</Button>
                <Button className="btn btn-danger">Delete</Button>
                </Col>:null
                }
                <div className="w-100"></div>
                <Col>
                    <DetailCard data={detail} image={image} text={text}/>
                    <div className="w-100"></div>
                </Col>
            </Row>
        </Container>
    )
};

export default EventDetail;