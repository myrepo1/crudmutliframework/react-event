import React from 'react';
import { Container,Row } from 'react-bootstrap';

import Forms from '../../Components/Forms';

const EventForm = (props)=>{
    const fields = [
        {name:'name',type:'text'},
        {name:'shortname',type:'text'},
        {name:'start',type:'datetime'},
        {name:'end',type:'datetime'},
        {name:'location',type:'text'},
        {name:'description',type:'text'}
    ];

    return (
        <Container>
            <Row><h3>Event Form</h3></Row>
            <Row>
                <Forms fields={fields} formAction={()=>{}}/>
            </Row>
        </Container>
    )
};

export default EventForm;