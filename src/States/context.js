import {createContext} from 'react';

const StateManager = createContext(null);

export default StateManager;