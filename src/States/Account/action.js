import {
    SUCCESS_GET_TOKEN,
    FAILED_GET_TOKEN,
    NO_TOKEN
} from '../types';

export const successGetToken = (token,id,is_staff)=> {
    const action = {token,id,is_staff,type:SUCCESS_GET_TOKEN}
    return action;
};

export const failedGetToken = () => {
    const action = {type:FAILED_GET_TOKEN};
    return action;
}

export const noToken = ()=> {
    const action = {type:NO_TOKEN};
    return action;
};