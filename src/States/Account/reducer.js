import {
    SUCCESS_GET_TOKEN,
    FAILED_GET_TOKEN,
    NO_TOKEN
} from '../types';

const local_account = localStorage.getItem('account');

const initial_state = local_account?JSON.parse(local_account):{token:null,id:null,is_staff:false};

const accountReducer = (state,action) =>{
    switch(action.type) {
      case SUCCESS_GET_TOKEN:
        return {...state, token:action.token,id:action.id,is_staff:action.is_staff};
      case FAILED_GET_TOKEN:
        return {...state, token:null, id:null};
      case NO_TOKEN:
        return {...state, token:null, id:null,is_staff:false};
      default:
        return state;
    }
  }

const getAccountReducer = ()=>{
    return [accountReducer,initial_state];
};

export default getAccountReducer;