import React, {useState} from 'react';
import {Form,FormGroup, FormLabel, FormControl,Button} from 'react-bootstrap';
import {Redirect} from 'react-router-dom';
import DatePicker from 'react-datepicker';

function toTitleCase(text) {
    // Convert text to Title Case
    const split_text = text.split(' ');
    const result = split_text.reduce((acc,val)=>(acc+val[0].toUpperCase() + val.slice(1)),'');
    return result;
}

function getInitialState(fields,useInitial) {
    // Get Initial State of all fields
    const result = {};
    if(useInitial) {
        fields.forEach((item)=>{result[item.name]=item.initial})
    }
    return result;
}

function ConvertDateString(value,is_date=false) {
    if(is_date) {
        let day = ''+value.getDate();
        let month = ''+(value.getMonth()+1);
        const year = ''+value.getFullYear();
        let hour = ''+value.getHours();
        let minute = ''+value.getMinutes();
        if(month.length<2){
            month = '0'+month;
        }
        if(day.length<2) {
            day = '0'+day;
        }
        if(hour.length<2){
            hour = '0'+hour;
        }
        if(minute.length<2) {
            minute = '0'+minute
        }
        const result = `${day}-${month}-${year} ${hour}:${minute}`;
        return result;
    } else {
        const format = /^(\d{2})-(\d{2})-(\d{4}) (\d{2}):(\d{2})$/g;
        const regex_result = [...value.matchAll(format)][0];
        if(regex_result) {
            const day = parseInt(regex_result[1],10);
            const month = parseInt(regex_result[2],10)-1;
            const year = parseInt(regex_result[3],10);
            const hour = parseInt(regex_result[4],10);
            const minute = parseInt(regex_result[5],10);
            return new Date(year,month,day,hour,minute);
        } else {
            return new Date();
        }
    }
}

const FormInput =(props)=> {
    const title = toTitleCase(props.field.name);
    const value = props.form_value[props.field.name];
    if(props.field.type==='datetime') {
        return (
            <FormGroup controlId={`formBasic${props.field.name.toUpperCase()}`}>
                <FormLabel>{title}</FormLabel>
                <DatePicker
                    showTimeSelect
                    onChange={(date)=>props.handleChange({target:{name:props.field.name,value:ConvertDateString(date,true)}})}
                    dateFormat="dd-MM-yyyy HH:mm"
                    timeFormat="HH:mm"
                    selected={value?ConvertDateString(value):null}
                    className="form-control"
                />
            </FormGroup>
        )
    } else {
        return (
            <FormGroup controlId={`formBasic${props.field.name.toUpperCase()}`}>
                <FormLabel>{title}</FormLabel>
                <FormControl 
                    type={props.field.type}
                    onChange={props.handleChange}
                    name={props.field.name}
                    value={value?value:''}
                    isValid={props.validated&&!props.error(props.field.name)}
                    isInvalid={props.validated&&props.error(props.field.name)}
                />
                <FormControl.Feedback type="invalid">{props.error(props.field.name)}</FormControl.Feedback>
            </FormGroup>
        )
    }
};

const Forms = (props)=>{
    const [formstate,setFormState] = useState(getInitialState(props.fields,props.useInitial));
    const [validated,setValidated] = useState(false);
    const [errors,setErrors] = useState({});
    const [redirect,setRedirect] = useState(false);

    const handleChange = (event)=>{
        setFormState({...formstate,[event.target.name]:event.target.value});
    };

    const handleSubmit = async (event)=>{
        event.preventDefault();
        const result = await props.formAction(formstate);
        if(result) {
            const error_message = {null:result.message,...result.detail};
            setErrors(error_message);
            setValidated(true);
        } else {
            setRedirect(true);
        }
    };

    const getError =(field)=>{
        return errors[field];
    };
    if(redirect&&props.redirection) {
        return <Redirect to={props.redirection}/>
    } else {
        return(
            <Form onSubmit={handleSubmit} noValidate>
                {getError(null)?<div className="alert alert-danger">{getError(null)}</div>:null}
                {props.fields.map((field,index)=>(
                <FormInput 
                    field={field}
                    handleChange={handleChange}
                    form_value={formstate}
                    key={index}
                    error={getError}
                    validated={validated}
                />
                ))}
                <Button type="submit">Submit</Button>
            </Form>
        )
    }
};

export default Forms;