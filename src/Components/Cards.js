import React from 'react';
import {Row,Col,Card} from 'react-bootstrap';

export const DisplayCard =(props)=>{
    return (
        <Col sm={12} md={6} lg={4}>
            <Card>
            {props.image?<Card.Img variant="top" src={props.image}/>:null}
                <Card.Body>
                    <Card.Title>{props.title}</Card.Title>
                    {props.text.map((item,index)=><Card.Text key={index}>{props.data[item]}</Card.Text>)}
                    {props.buttons?
                    <Row className="justify-content-around">
                        {props.buttons.map((item,index)=><a className={item.className} href={item.link} key={index}>{item.name}</a>)}
                </Row>:null
                    }
                </Card.Body>
            </Card>
        </Col>
    )
};

export const DetailCard = (props)=>{
    return (
        <Card>
        {props.image?<Card.Img variant="top" src={props.image}/>:null}
            <Card.Body>
                <Card.Title>{props.title}</Card.Title>
                {props.text.map((item,index)=><Card.Text key={index}>{props.data[item]}</Card.Text>)}
            </Card.Body>
        </Card>
    )
};